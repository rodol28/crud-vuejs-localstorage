const app = new Vue({
  el: "#app",
  data: {
    title: "CRUD Vue.js",
    tasks: [],
    newTask: "",
  },
  methods: {
    addTask() {
      let id = Math.random().toString(36).substr(2, 9);
      this.tasks.push({
        id,
        description: this.newTask,
        state: false,
      });
      this.saveAtLocalStorage();
      this.newTask = "";
    },
    changeState(id) {
      const index = this.tasks.findIndex(task => task.id === id);
      this.tasks[index].state = !this.tasks[index].state;
    },
    deleteTask(id) {
      const index = this.tasks.findIndex(task => task.id === id);
      this.tasks.splice(index, 1);
      this.saveAtLocalStorage();
    },
    saveAtLocalStorage() {
      localStorage.setItem("tasks-vue", JSON.stringify(this.tasks));
    },
  },
  created: function () {
    let data = JSON.parse(localStorage.getItem("tasks-vue"));
    if (data === null) return (this.tasks = []);
    this.tasks = data;
  },
});
